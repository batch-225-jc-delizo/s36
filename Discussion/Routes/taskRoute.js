// Contains all the endpoints for our application

// We need to use express Router() function to achieve this 

const express = require ("express");

//Creates a Router instance that function as a middleware and routing system
// Allow access to HTTP method middlewares that make it easier to create for our application
const router = express.Router();


const taskController = require("../controllers/taskController");

/*
     syntax: localhost:3001/tasks/getinfo

*/

// ROUTES
// Route to get all the task
router.get("/getinfo", (req, res) => {

    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})
// Use "module.exports" to export the router objects to use in the "app.js"

// Route to create a new task
router.post("/create", (req, res) => {

	// If the information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	taskController.createTask(req.body).then(result => res.send(result));
})

// Route to delete a task
// The colon (:) is anidentifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
/*
	Ex. 
	localhost: 3001/tasks/delete/1234
	the 1234 is assigned to the "id"
	parameter in the URL
*/

router.delete("/delete/:id", (req, res) => {

	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

// Route to update a task
router.put("/update/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

//ACTIVITY
router.put("/:id", (req, res) => {

    taskController.updateStatus(req.params.id ,req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) => {
    const id = req.params.id;
    taskController.getTask(id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;

