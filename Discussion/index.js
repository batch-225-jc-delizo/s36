// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const taskRoute = require("./routes/taskRoute")

// Server setup 
const app = express();
const port = 3002;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connect to MongoDB Atlas
mongoose.connect(`mongodb+srv://jcdelizo:${process.env.PASSWORD}@cluster0.y6g90au.mongodb.net/MRC?retryWrites=true&w=majority`,
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	});

// Setup notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console,"Connection error"));
db.on("open", () => console.log('Connected to MongoDB!'));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}!`));


