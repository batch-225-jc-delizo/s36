// Contains all the endpoints for our application

// We need to use express Router() function to achieve this 

const express = require ("express");

//Creates a Router instance that function as a middleware and routing system
// Allow access to HTTP method middlewares that make it easier to create for our application
const router = express.Router();


const taskController = require("../controllers/taskController");

/*
     syntax: localhost:3001/tasks/getinfo

*/

// ROUTES
router.get("/getinfo", (req, res) => {

    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})
// Use "module.exports" to export the router objects to use in the "app.js"


module.exports = router;