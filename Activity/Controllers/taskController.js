//Controller contains the functions and Business logic of our express JS. Meaning all the operation it can do will be placed in this file.

const Task = require("../models/task")

module.exports.getAllTasks = () => {

    //The "return" statement. returns the result of the Mongoose method
    //Then "then" method is used to wait for the Mongoose method to finish before sending the result back to the route

    return Task.find({}).then(result => {
        return result;
    })
}